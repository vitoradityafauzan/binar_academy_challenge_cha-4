// Import core module http
const http = require("http");
// Import core module file system
const fs = require("fs");
// Import core module path
const path = require("path");
// Import third party module mime
const mime = require("mime");

// Set port for server
const { PORT = 8000 } = process.env;

// Import Cars JSON
const carsApi = require("../data/cars.min.json");

// variable to direct towars public directory
const PUBLIC_DIRECTORY = path.join(__dirname, "..", "public");

// START function to open/read html file
function getHtml(htmlFile) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFile);
  return fs.readFileSync(htmlFilePath, "utf-8");
}
// END

// START routing
function onRequest(req, res) {
  // if (
  //   req.url === "/" ||
  //   req.url.includes("driver") ||
  //   req.url.includes("date") ||
  //   req.url.includes("time") ||
  //   req.url.includes("capacity")
  // ) {
  //   const html = getHtml("index.html"); // get html file
  //   res.writeHead(200, { "Content-Type": "text/html" });
  //   res.end(html);
  // } else if (
  //   req.url === "/search-car" ||
  //   req.url.includes("driver") ||
  //   req.url.includes("date") ||
  //   req.url.includes("time") ||
  //   req.url.includes("capacity")
  // ) {
  // }
  if (req.url === "/" || req.url === "/home") {
    const html = getHtml("index.html"); // get html file
    res.writeHead(200, { "Content-Type": "text/html" });
    res.end(html);
  } else if (req.url === "/cars-search") {
    const html = getHtml("search.html"); // get html file
    res.writeHead(200, { "Content-Type": "text/html" });
    res.end(html);
  } else if (req.url === "/api/cars") {
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify(carsApi));
  } else if (req.url.match(".css$") || req.url.match(".js$")) {
    const filePath = path.join(__dirname, "..", "public", req.url);
    const fileStream = fs.createReadStream(filePath, "UTF-8");
    const mimeType = mime.getType(filePath); // get mime type
    res.writeHead(200, { "Content-Type": mimeType });
    fileStream.pipe(res);
  } else if (req.url.match(".jpg$") || req.url.match(".png$")) {
    const filePath = path.join(__dirname, "..", "public", req.url);
    const fileStream = fs.createReadStream(filePath);
    const mimeType = mime.getType(filePath); // get mime type
    res.writeHead(200, { "Content-Type": mimeType });
    fileStream.pipe(res);
  }
  // else if (req.url.match(".png$")) {
  //   const filePath = path.join(__dirname, "..", "public", req.url);
  //   const fileStream = fs.createReadStream(filePath, "UTF-8");
  //   const mimeType = mime.getType(filePath); // get mime type
  //   res.writeHead(200, { "Content-Type": mimeType });
  //   fileStream.pipe(res);
  // } else if (req.url.match(".jpeg$")) {
  //   const filePath = path.join(__dirname, "..", "public", req.url);
  //   const fileStream = fs.createReadStream(filePath, "UTF-8");
  //   const mimeType = mime.getType(filePath); // get mime type
  //   res.writeHead(200, { "Content-Type": mimeType });
  //   fileStream.pipe(res);
  // }
  else {
    res.writeHead(404, { "Content-Type": "text/html" });
    res.end("No Page Found");
  }
}
// END

// Syntax for creating server
const server = http.createServer(onRequest);

// Syntax for running server
server.listen(PORT, "0.0.0.0", () => {
  console.log(`Server is Listening on port ${PORT}`);
});
