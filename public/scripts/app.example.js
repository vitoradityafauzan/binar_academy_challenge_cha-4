class App {
  constructor() {
    this.clearButton = document.getElementById("clear-search");
    // this.loadButton = document.getElementById("load-search");
    this.carContainerElement = document.getElementById("car-container");
    this.searchButton = document.getElementById("search-all");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.searchButton.onclick = this.run;
  }

  run = () => {
    this.clear();
    // let date2 = new Date(`${date}`);
    // let time2 = new Date(`${time}:00`);
    // var dateTime = new Date(document.getElementById("availableAt").value);
    // new Date(`${dateCari}T${timeCari}Z`);
    // var content = document.getElementById("car-container");

    let date = document.getElementById("availableAt").value;
    let time = document.getElementById("time").value;
    let dateTime = new Date(`${date}T${time}:00Z`);
    let date2 = dateTime.getFullYear() + dateTime.getMonth() + dateTime.getDate();
    let time2 = dateTime.getHours() + dateTime.getMinutes();

    let jmlhPenumpang = document.getElementById("capacity").value;

    let today = new Date();
    let currentDate = today.getFullYear()+today.getMonth()+today.getDate();

    console.log(date);
    console.log(time);
    console.log(dateTime);
    console.log(currentDate);
    console.log(date2);
    console.log(time2);
    
    
      // console.log(carDate)
      if (
        date === "" &&
        time === "" &&
        jmlhPenumpang === ""
      ) {
        alert("Harap Diisikan Semua Field!");
      } else {
        this.clear();
        if (date2 !== "" && date2 < currentDate) {
          alert("Tanggal Harus Lebih Besar Dibanding Tanggal Sekarang !");
          // console.log("Empty")
          this.clear();
        } else {
          Car.list.forEach((car) => {
            var carDate =
              car.availableAt.getFullYear() + 
              car.availableAt.getMonth() + 
              car.availableAt.getDate();

            let carTime = car.availableAt.getHours()+car.availableAt.getMinutes();
            console.log(carTime);
            console.log(time2);
            if (
              date2 > carDate &&
              time2 > carTime &&
              jmlhPenumpang <= car.capacity
            ) {
              console.log(time2)
              console.log(carTime)
              const node = document.createElement("div");
              node.innerHTML = car.render();
              this.carContainerElement.append(node);
            }
          });
        }
      }

        // if (date !== "" && time === "" && jmlhPenumpang === "") {
        //   if (date2 < currentDate) {
        //     alert("Tanggal Harus Lebih Besar Dibanding Tanggal Sekarang !");
        //   } else {
        //     if (date2 > carDate) {
        //       const node = document.createElement("div");
        //       node.innerHTML = car.render();
        //       this.carContainerElement.append(node);
        //     }
        //   }
        // } else if (date !== "" && time !== "" && jmlhPenumpang === "") {
        //   if (date2 < currentDate) {
        //     alert("Tanggal Harus Lebih Besar Dibanding Tanggal Sekarang !");
        //   } else {
        //     if (date2 > carDate && time2 > carTime) {
        //       const node = document.createElement("div");
        //       node.innerHTML = car.render();
        //       this.carContainerElement.append(node);
        //     }
        //   }
        // } else if (date !== "" && time !== "" && jmlhPenumpang !== "") {
        //   if (date2 < currentDate) {
        //     alert("Tanggal Harus Lebih Besar Dibanding Tanggal Sekarang !");
        //   } else {
        //     if (
        //       date2 > carDate &&
        //       time2 > carTime &&
        //       jmlhPenumpang >= car.capacity
        //     ) {
        //       const node = document.createElement("div");
        //       node.innerHTML = car.render();
        //       this.carContainerElement.append(node);
        //     }
        //   }
        // } else {
        //   this.clear();
        // }
    
    

    /*
    if (date === "" || time === "" || jmlhPenumpang === "") {
      alert("Semua Field Harus Diisi");
    } else {
      this.clear();
      Car.list.forEach((car) => {
        // console.log(dateTime)

        // var carDateTime = car.availableAt.getFullYear()+"-"+car.availableAt.getMonth()+"-"+
        if (
          //dateTime >= car.availableAt &&
          car.available === true &&
          jmlhPenumpang <= car.capacity
        ) {
          console.log(car.availableAt.getFullYear());
          console.log(jmlhPenumpang);
          console.log(car.capacity);
          console.log(car.available);
          const node = document.createElement("div");
          node.innerHTML = car.render();
          this.carContainerElement.append(node);
        }
      });
    } */
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
