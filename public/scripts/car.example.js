class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(rentPerDay);
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="card" style="width: 400px; margin: 50px 25px;">
        <img
          class="card-img-top"
          src="${this.image}"
          alt="${this.manufacture}"
          id="card-img"
          style="height: 250px;"
        />
        <div class="card-body">
          <h4 class="card-title">${this.plate}</h4>
          <p class="card-text"><i class="fa-solid fa-car-rear"></i> <b>Car ID:</b> ${this.id}</p>
          <p class="card-text"><b>By</b> ${this.manufacture} <b>With Model</b> ${this.model}</p>
          <p class="card-text"><b>Available At:</b> ${this.availableAt}</p>
          <p class="card-text">${this.description}</p>
          <p class="card-text"><b>Penumpang:</b> ${this.capacity}</p>
          <p class="card-text"><b>Harga: </b> ${this.rentPerDay}</p>
          <a href="#" class="btn btn-primary">
            See Profile
          </a>
        </div>
      </div>
    `;
  }
}
/*
`<div class="card" style="width: 400px">
      <img
        class="card-img-top"
        src="${this.image}"
        alt="${this.manufacture}"
      />
      <div class="card-body">
        <h4 class="card-title">${this.plate}</h4>
        <p class="card-text">Car ID: ${this.id}</p>
        <p class="card-text">Manufacturer: ${this.manufacture}</p>
        <p class="card-text">Model: ${this.model}</p>
        <p class="card-text">Available At: ${this.availableAt}</p>
        <a href="#" class="btn btn-primary">
          See Profile
        </a>
      </div>
    </div>`
*/
